/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package battleship;

import com.sun.prism.paint.Color;
import javax.swing.JFrame;
import javax.swing.JButton;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.FlowLayout;
import java.util.ArrayList;
import java.util.Random;
import javax.swing.JTextField;

import java.util.Scanner;
import java.net.ServerSocket;
import java.net.Socket;
import java.io.PrintStream;
import java.net.InetAddress;
import java.net.SocketAddress;
import java.net.UnknownHostException;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.awt.Container;
import javax.swing.ButtonGroup;
import javax.swing.JLabel;
import javax.swing.JRadioButton;

/**
 *
 * @author ftonye
 */
public class BattleSea extends JFrame
{
    private static JButton[] listbtn;
    private static ArrayList<Ship> listShip;
    private static ArrayList<JButton> listJButtonInShip;
    // adding new j frame and radio buttons
    private JFrame ourframe = new JFrame("Make your Selection");
     JRadioButton bt1 = new JRadioButton("Client");
     JRadioButton bt2 = new JRadioButton("Serveur");
     ButtonGroup group = new ButtonGroup();
     JButton btselect = new JButton("Select");
     JTextField text1 = new JTextField();
     JTextField textport = new JTextField();
     JTextField text2 = new JTextField();
     
    private InetAddress adrs;
    
    private JButton btnStartServer;
    private JTextField txtPortNumber;
    private BackGroundCom com;
    private Thread t;
   /*
    public void popupform(){
        ourframe.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        ourframe.setBounds(200, 100, 400, 200);
        
        Container container = ourframe.getContentPane();
        container.setLayout(null);
        
        JLabel logo = new JLabel("Connect as a client or server ");
        logo.setBounds(60,5,250,30);
        
        container.add(logo);
        
        ourframe.setVisible(true);
    } */
    {
       
        this.setSize(500,425);
        try {
            adrs = InetAddress.getLocalHost();
        } catch (UnknownHostException ex) {
            Logger.getLogger(BattleSea.class.getName()).log(Level.SEVERE, null, ex);
        }
       
        this.setTitle("BattleSea Server address is: " + adrs.getHostAddress());
        this.setLayout(new FlowLayout());
        
        createGrid();
        linkListenerToSeaSector();
               
        this.add(btnStartServer);
        this.add(txtPortNumber);

         CreateShips();
         
        ourframe.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        ourframe.setBounds(201, 201, 599, 460);
        
        Container container = ourframe.getContentPane();
        container.setLayout(null);
        
        JLabel logo = new JLabel("Connect as a client or server ");
        JLabel ipadd = new JLabel("ENTER YOUR IP ADDRESS");
        JLabel portid = new JLabel("ENTER YOUR PORT");
        
        logo.setBounds(60,5,250,30);
        bt1.setBounds(60, 30, 260, 30);
        bt2.setBounds(60, 55, 260, 30);
        btselect.setBounds(75,90,260,30);

        portid.setBounds(100, 140, 200, 100);
        ipadd.setBounds(100,140,250,30);
        text1.setBounds(250,140,90,30);
        textport.setBounds(250,180,90,30);
        JLabel selection = new JLabel("");
        
  
       
        
        selection.setBounds(90,120,250,30);
        selection.setBounds(90,120,250,30);           
        group.add(bt1);
        group.add(bt2);
        container.add(bt1);
        container.add(bt2);
        container.add(logo);
        container.add(btselect);
        container.add(selection);
        container.add(text1);
        container.add(ipadd);
        container.add(portid);
        container.add(textport);
        
        ipadd.setVisible(false);
        ipadd.setVisible(false);
         btselect.addActionListener(new ActionListener()
         {
             @Override
            public void actionPerformed(ActionEvent clicked) 
            {
                // si client est selectioner
               if(bt1.isSelected())
               {
                  selection.setText("Bienvenue Client");
                  
               }    
               else if (bt2.isSelected())
               {
                   selection.setText("Bienvenue Server");
               }
            }
         });
                 
        ourframe.setVisible(true);
        
    }
    
    
    private void createGrid()
    {
        listbtn = new JButton[100];
        
        for(int i = 0; i< listbtn.length;i++)
        {
         listbtn[i] = new JButton(String.valueOf(i)); 
         listbtn[i].setSize(10,10);
         listbtn[i].setBackground(java.awt.Color.blue);          
         this.add(listbtn[i]);
        
        }
        
        btnStartServer = new JButton("Start Server");
        btnStartServer.addActionListener(new ActionListener()
        {
            @Override
            public void actionPerformed(ActionEvent ae) {
               com = new BackGroundCom(Integer.parseInt(txtPortNumber.getText().toString()));
              t = new Thread(com);
               t.start();
            }
        
        });
        txtPortNumber = new JTextField();
        txtPortNumber.setText("Enter Port number");
    }
    // event listenser 
    private void linkListenerToSeaSector()
    {
        for(int i = 0; i<listbtn.length;i++)
        {
             listbtn[i].addActionListener(new ActionListener()
          {
             @Override
             public void actionPerformed(ActionEvent ae) 
             {
                
                com.missileOutgoing = Integer.parseInt(ae.getActionCommand());
                com.dataToSend = true;
                 
                  
             }    
          });
        }
         
    }
    
   
       // create ships 
    private void CreateShips()
    {
        int head ,nb = 0;
        Random r = new Random();
        listShip = new ArrayList<Ship>();
        listJButtonInShip = new ArrayList<JButton>();
        
        
        for(int i = 0;i<5;i++)
        {
            try
            {
                 ArrayList<JButton> boatsection = new ArrayList<JButton>();
                 head = r.nextInt(98)+1;
                 nb = r.nextInt(3)+1;
                 
                 
                 for(int cnt=0;cnt< nb ;cnt++)
                 {
                   boatsection.add(listbtn[head+cnt]);
                   listJButtonInShip.add(listbtn[head+cnt]);
                 }
                 
                 Ship s = new Ship(boatsection);
                 listShip.add(s);
                
            }
            catch(Exception ex)
            {
                
            }
           
         
            
        }
        
        
    
    }
    
    public static void UpdateGrig(int incomming)
    {
        Boolean missHit = false;
        
        for(int i = 0; i<listbtn.length;i++)
        {
           
                
                  for(Ship element:listShip)
                  {
                      element.checkHit(listbtn[incomming]);
                     
                  }  
                 
                  
                
          
        }
        
        for(JButton element:listJButtonInShip)
        {
            if(!element.equals(listbtn[incomming]))
                missHit = true;
        }

         if(missHit)
         {
             listbtn[incomming].setBackground(java.awt.Color.magenta); 
         }
    }

    
}
